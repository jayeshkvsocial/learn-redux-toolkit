import React from "react";
import Home from "./Home/Home";
import Header from "./Header/Header";
import { store } from "./Redux/Store/Reduxstore";
import { Provider } from "react-redux";
import app from "./App.module.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

const App = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <div>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
          </Routes>
        </div>
      </Provider>
    </BrowserRouter>
  );
};

export default App;
