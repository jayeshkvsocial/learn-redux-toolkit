import React from "react";
import classes from "../../styles/Navigation.module.css";
import { Link } from "react-router-dom";

const Navigation = () => {
  return (
    <div>
      <nav>
        <ul className={classes.ul}>
          <li className={classes.li}>
            <Link to="/Home">Home</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navigation;
